$(document).ready(function() {

  $('.s-burger').on('click', function () {
    if ( !$('.s-main-nav__list').hasClass('s-main-nav__list--is-active') ) {
      $('.s-main-nav__list').addClass('s-main-nav__list--is-active');
    } else {
      $('.s-main-nav__list').removeClass('s-main-nav__list--is-active');
    }
  });

  $('.s-promo__slider').owlCarousel({
    items: 1
  });

  $('.s-partners__slider').owlCarousel({
    mouseDrag: false,
    responsive: {
      0: {
        items: 1
      },
      480: {
        items: 2
      },
      768: {
        items: 3
      },
      992: {
        items: 5
      }
    }
  });

});